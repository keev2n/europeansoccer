﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EuropeanSoccerPrediction
{
    class Player
    {
        public int playerId { get; set; }
        public float overAllRating { get; set; }
        public Player(int playerId, float overAllRating)
        {
            this.playerId = playerId;
            this.overAllRating = overAllRating;
        }
    }
}
