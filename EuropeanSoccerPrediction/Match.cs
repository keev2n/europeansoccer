﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace EuropeanSoccerPrediction
{
    class Match : IComparable<Match>
    {
        public static List<Match> statistics = new List<Match>();
        public float resultHome { get; set; }
        public float resultDraw { get; set; }
        public float resultAway { get; set; }
        public int matchId { get; }
        public List<Player> team1 { get; set; }
        public List<Player> team2 { get; set; }
        public DateTime time { get; set; }
        public float betOddsHome { get; }
        public float betOddsDraw { get; }
        public float betOddsAway { get; }
        public int goalHome { get; }
        public int goalAway { get; }
        public float predictedResult { get; set; }
        public Match(int matchId, int goalHome, int goalAway, float betOddsHome, float betOddsDraw, float betOddsAway, DateTime time)
        {
            team1 = new List<Player>();
            team2 = new List<Player>();
            this.matchId = matchId;
            this.goalHome = goalHome;
            this.goalAway = goalAway;
            this.betOddsHome = betOddsHome;
            this.betOddsDraw = betOddsDraw;
            this.betOddsAway = betOddsAway;
            this.time = time;
            calculateResult();
        }
        public int CompareTo(Match other)
        {
            return matchId.CompareTo(other.matchId);
        }
        private void calculateResult()
        {
            if (goalHome > goalAway)
            {
                resultHome = 1;
                resultDraw = 0;
                resultAway = 0;

            }
            else if (goalHome < goalAway)
            {
                resultAway = 1;
                resultDraw = 0;
                resultHome = 0;
            }
            else
            {
                resultDraw = 1;
                resultHome = 0;
                resultAway = 0;
            }
                
        }
        public override string ToString()
        {
            return $"Match Id: {matchId}, BetOddsHome: {betOddsHome}, BettOdsDraw: {betOddsDraw}, BetOddsAway: {betOddsAway}, goalHome: {goalHome} goalAway: {goalAway}";
        }
    }
}
