﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace EuropeanSoccerPrediction
{
    class LoadAndCreateDatas
    {
        static void LoadFromDatabase()
        {
            int db = 0;
            string cs = "database.sqlite";

            var connectionstring = new SQLiteConnectionStringBuilder();
            connectionstring.DataSource = cs;

            using (var con = new SQLiteConnection(connectionstring.ConnectionString))
            {
                con.Open();

                var selectCmd = con.CreateCommand();
                selectCmd.CommandText = "SELECT ID, home_team_goal, away_team_goal, B365H, B365D, B365A, home_player_1, home_player_2, home_player_3, " +
                    "home_player_4, home_player_5, home_player_6, home_player_7, home_player_8, home_player_9, home_player_10, home_player_11, " +
                    "away_player_1, away_player_2, away_player_3, away_player_4, away_player_5, away_player_6, away_player_7, away_player_8, away_player_9, " +
                    "away_player_10, away_player_11, date  FROM Match";
                using (var reader = selectCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(3) && !reader.IsDBNull(6) && !reader.IsDBNull(7) && !reader.IsDBNull(8) && !reader.IsDBNull(9) && !reader.IsDBNull(10) && !reader.IsDBNull(11) &&
                            !reader.IsDBNull(12) && !reader.IsDBNull(13) && !reader.IsDBNull(14) && !reader.IsDBNull(15) && !reader.IsDBNull(16) && !reader.IsDBNull(17) && !reader.IsDBNull(18) &&
                            !reader.IsDBNull(19) && !reader.IsDBNull(20) && !reader.IsDBNull(21) && !reader.IsDBNull(22) && !reader.IsDBNull(23) && !reader.IsDBNull(24) && !reader.IsDBNull(25) &&
                            !reader.IsDBNull(26) && !reader.IsDBNull(27))
                        {
                            db++;
                            var id = reader.GetInt32(0);
                            var goalHome = reader.GetInt32(1);
                            var goalAway = reader.GetInt32(2);
                            var betOddsHome = reader.GetFloat(3);
                            var betOddsDraw = reader.GetFloat(4);
                            var betOddsAway = reader.GetFloat(5);
                            var DateTime = reader.GetDateTime(28);

                            Match stat = new Match(id, goalHome, goalAway, betOddsHome, betOddsDraw, betOddsAway, DateTime);
                            for (int i = 6; i < 17; i++)
                            {
                                Player p = new Player(reader.GetInt32(i), 0);
                                stat.team1.Add(p);
                            }

                            for (int i = 17; i < 28; i++)
                            {
                                Player p = new Player(reader.GetInt32(i), 0);
                                stat.team2.Add(p);
                            }

                            Match.statistics.Add(stat);
                        }
                    }
                }

                selectCmd.CommandText = $"SELECT player_api_id, crossing, finishing, heading_accuracy, short_passing, volleys, dribbling, curve," +
                    $" free_kick_accuracy, long_passing, ball_control, acceleration, sprint_speed, agility, reactions, balance, shot_power, jumping, stamina, strength," +
                    $" long_shots, aggression, interceptions, positioning, vision, penalties, marking, standing_tackle, sliding_tackle, gk_diving, gk_handling," +
                    $" gk_kicking, gk_positioning, gk_reflexes, date, overall_rating from Player_Attributes";
                using (var reader = selectCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(0) && !reader.IsDBNull(1) && !reader.IsDBNull(2) && !reader.IsDBNull(3) && !reader.IsDBNull(4) && !reader.IsDBNull(5) && !reader.IsDBNull(6) && !reader.IsDBNull(7) && !reader.IsDBNull(8) && !reader.IsDBNull(9) && !reader.IsDBNull(10) && !reader.IsDBNull(11) &&
                            !reader.IsDBNull(12) && !reader.IsDBNull(13) && !reader.IsDBNull(14) && !reader.IsDBNull(15) && !reader.IsDBNull(16) && !reader.IsDBNull(17) && !reader.IsDBNull(18) &&
                            !reader.IsDBNull(19) && !reader.IsDBNull(20) && !reader.IsDBNull(21) && !reader.IsDBNull(22) && !reader.IsDBNull(23) && !reader.IsDBNull(24) && !reader.IsDBNull(25) &&
                            !reader.IsDBNull(26) && !reader.IsDBNull(27) && !reader.IsDBNull(28) && !reader.IsDBNull(29) && !reader.IsDBNull(30) && !reader.IsDBNull(31) && !reader.IsDBNull(32)
                            && !reader.IsDBNull(33) && !reader.IsDBNull(34) && !reader.IsDBNull(35))
                        {
                            float[] properties = new float[33];
                            float overall_rating = 0;
                            var id = reader.GetInt32(0);
                            for (int i = 1; i < 34; i++)
                            {
                                properties[i - 1] = reader.GetFloat(i);
                            }
                            var bestEight = (from x in properties
                                             orderby x descending
                                             select x).Take(18).ToList();
                            overall_rating = bestEight.Average();
                            var date = reader.GetDateTime(34);
                            HelperPlayerAttributes helperPlayerAttributes = new HelperPlayerAttributes(id, overall_rating, date);
                            HelperPlayerAttributes.helperPlayerAttributes.Add(helperPlayerAttributes);
                        }
                    }
                }
                Console.WriteLine("Teszt");

                Stopwatch test = new Stopwatch();
                test.Start();
                int db2 = -1;
                int db3 = Match.statistics.Count();
                foreach (var Match in Match.statistics)
                {
                    db2++;
                    foreach (var player1 in Match.team1)
                    {
                        float overallRateAvarage = 0;
                        int piece = 0;
                        foreach (var helper in HelperPlayerAttributes.helperPlayerAttributes)
                        {
                            if (player1.playerId == helper.PlayerApiID)
                            {
                                if (Match.time.Year - 2 < helper.Time.Year && Match.time.Year + 2 > helper.Time.Year)
                                {
                                    overallRateAvarage += helper.OverallRating;
                                    piece++;
                                }
                                if (test.ElapsedMilliseconds > 30000)
                                {
                                    Console.WriteLine($"Still loading {db2}, {db3}");
                                    test.Restart();
                                }
                            }
                        }
                        player1.overAllRating = overallRateAvarage / piece;
                        if (player1.overAllRating == 0)
                        {
                            Console.WriteLine($"HIBA az ID:{player1.playerId}");
                        }

                    }

                    foreach (var player2 in Match.team2)
                    {
                        float overallRateAvarage = 0;
                        int piece = 0;
                        foreach (var helper in HelperPlayerAttributes.helperPlayerAttributes)
                        {
                            if (player2.playerId == helper.PlayerApiID)
                            {
                                if (Match.time.Year - 2 < helper.Time.Year && Match.time.Year + 2 > helper.Time.Year)
                                {
                                    overallRateAvarage += helper.OverallRating;
                                    piece++;
                                }
                                if (test.ElapsedMilliseconds > 30000)
                                {
                                    Console.WriteLine($"Still loading {db2}, {db3}");
                                    test.Restart();
                                }
                            }
                        }
                        player2.overAllRating = overallRateAvarage / piece;
                        if (player2.overAllRating == 0)
                        {
                            Console.WriteLine($"HIBA az ID:{player2.playerId}");
                        }
                    }
                }
            };

            Console.WriteLine(db);

            ToTheTxt();
        }
        static void ToTheTxt()
        {
            StreamWriter sw = new StreamWriter("data18.txt");

            foreach (var item in Match.statistics)
            {
                List<float> team1ListRating = new List<float>();
                List<float> team1ListID = new List<float>();
                List<float> team2ListRating = new List<float>();
                List<float> team2ListID = new List<float>();
                foreach (var team1 in item.team1)
                {
                    team1ListID.Add(team1.playerId);
                    team1ListRating.Add(team1.overAllRating);
                }
                sw.Write($"{item.goalHome};{item.goalAway};{item.matchId};{item.betOddsHome};{item.betOddsDraw};{item.betOddsAway};{item.time};");
                for (int i = 0; i < team1ListRating.Count(); i++)
                {
                    sw.Write($"{team1ListID[i]};{team1ListRating[i]};");
                }
                foreach (var team2 in item.team2)
                {
                    team2ListID.Add(team2.playerId);
                    team2ListRating.Add(team2.overAllRating);
                }
                for (int i = 0; i < team2ListRating.Count(); i++)
                {
                    sw.Write($"{team2ListID[i]};{team2ListRating[i]};");
                }
                sw.Write("\n");
            }
            sw.Close();
        }
        public static void SeperateDataAndTest(string name)
        {
            List<Match> matches = new List<Match>();
            StreamReader sr = new StreamReader(name+".txt");
            int db = 0;
            while (!sr.EndOfStream)
            {
                bool isNanOrNot = false;
                string[] row = sr.ReadLine().Split(";");
                int goalHome = int.Parse(row[0]);
                int goalAway = int.Parse(row[1]);
                int matchId = int.Parse(row[2]);
                float homeOdds = float.Parse(row[3]);
                float drawOdds = float.Parse(row[4]);
                float awayOdds = float.Parse(row[5]);
                DateTime time = DateTime.Parse(row[6]);
                Match match = new Match(matchId, goalHome, goalAway, homeOdds, drawOdds, awayOdds, time);
                for (int i = 0; i < 22; i += 2)
                {
                    int playerID = int.Parse(row[7 + i]);
                    float overAllRating = float.Parse(row[8 + i]);
                    if (float.IsNaN(overAllRating))
                    {
                        isNanOrNot = true;
                    }
                    Player p = new Player(playerID, overAllRating);
                    match.team1.Add(p);
                }
                for (int i = 0; i < 22; i += 2)
                {
                    int playerId = int.Parse(row[29 + i]);
                    float overAllRating = float.Parse(row[30 + i]);
                    if (float.IsNaN(overAllRating))
                    {
                        isNanOrNot = true;
                    }
                    Player p = new Player(playerId, overAllRating);
                    match.team2.Add(p);
                }
                if (isNanOrNot == false)
                {
                    matches.Add(match);
                    db++;
                }
            }
            Console.WriteLine(db);
            sr.Close();

            int countData = Convert.ToInt32(matches.Count() * 0.9);
            StreamWriter sw = new StreamWriter(name+".txt");
            StreamWriter swForTest = new StreamWriter(name + "ForTesting.txt");
            int counter = 0;
            foreach (var item in matches)
            {
                counter++;
                if (counter <= countData)
                {
                    List<float> team1ListRating = new List<float>();
                    List<float> team1ListID = new List<float>();
                    List<float> team2ListRating = new List<float>();
                    List<float> team2ListID = new List<float>();
                    foreach (var team1 in item.team1)
                    {
                        team1ListID.Add(team1.playerId);
                        team1ListRating.Add(team1.overAllRating);
                    }
                    sw.Write($"{item.goalHome};{item.goalAway};{item.matchId};{item.betOddsHome};{item.betOddsDraw};{item.betOddsAway};{item.time};");
                    for (int i = 0; i < team1ListRating.Count(); i++)
                    {
                        sw.Write($"{team1ListID[i]};{team1ListRating[i]};");
                    }
                    foreach (var team2 in item.team2)
                    {
                        team2ListID.Add(team2.playerId);
                        team2ListRating.Add(team2.overAllRating);
                    }
                    for (int i = 0; i < team2ListRating.Count(); i++)
                    {
                        sw.Write($"{team2ListID[i]};{team2ListRating[i]};");
                    }
                    sw.Write("\n");
                }
                else
                {
                    List<float> team1ListRating = new List<float>();
                    List<float> team1ListID = new List<float>();
                    List<float> team2ListRating = new List<float>();
                    List<float> team2ListID = new List<float>();
                    foreach (var team1 in item.team1)
                    {
                        team1ListID.Add(team1.playerId);
                        team1ListRating.Add(team1.overAllRating);
                    }
                    swForTest.Write($"{item.goalHome};{item.goalAway};{item.matchId};{item.betOddsHome};{item.betOddsDraw};{item.betOddsAway};{item.time};");
                    for (int i = 0; i < team1ListRating.Count(); i++)
                    {
                        swForTest.Write($"{team1ListID[i]};{team1ListRating[i]};");
                    }
                    foreach (var team2 in item.team2)
                    {
                        team2ListID.Add(team2.playerId);
                        team2ListRating.Add(team2.overAllRating);
                    }
                    for (int i = 0; i < team2ListRating.Count(); i++)
                    {
                        swForTest.Write($"{team2ListID[i]};{team2ListRating[i]};");
                    }
                    swForTest.Write("\n");
                }
            }
            sw.Close();
            swForTest.Close();
        }
    }
}
