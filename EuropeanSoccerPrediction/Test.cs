﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EuropeanSoccerPrediction
{
    class Test
    {
        public List<Match> idealTestDataMatches = new List<Match>();
        public List<Match> idealTestDataMatchesForTest = new List<Match>();
        public void CreateTestDatas()
        {
            Random r = new Random();

            for (int i = 0; i < 24000; i++)
            {
                int randomNumber = r.Next(0, 3);
                if (randomNumber == 0)
                {
                    int goalHome = 0;
                    int goalAway = 0;
                    while (goalHome <= goalAway)
                    {
                        goalHome = r.Next(1, 15);
                        goalAway = r.Next(1, 15);
                    }
                    float homeOdds = (float)r.NextDouble() + r.Next(1, 3);
                    float drawOdds = (float)r.NextDouble() + r.Next(3, 10);
                    float awayOdds = (float)r.NextDouble() + r.Next(3, 10);
                    Match match = new Match(i, goalHome, goalAway, homeOdds, drawOdds, awayOdds, DateTime.Now);
                    for (int j = 0; j < 11; j++)
                    {
                        Player p = new Player(j, r.Next(20, 50));
                        match.team2.Add(p);
                    }
                    for (int j = 0; j < 11; j++)
                    {
                        Player p = new Player(j, r.Next(50, 100));
                        match.team1.Add(p);
                    }
                    if (i < 22000)
                    {
                        idealTestDataMatches.Add(match);
                    }
                    else
                    {
                        idealTestDataMatchesForTest.Add(match);
                    }
                }
                else if (randomNumber == 1)
                {
                    int goalHome = 0;
                    int goalAway = 0;
                    while (goalHome != goalAway)
                    {
                        goalHome = r.Next(1, 15);
                        goalAway = r.Next(1, 15);
                    }
                    float drawOdds = (float)r.NextDouble() + r.Next(1, 3);
                    float homeOdds = (float)r.NextDouble() + r.Next(3, 10);
                    float awayOdds = (float)r.NextDouble() + r.Next(3, 10);
                    Match match = new Match(i, goalHome, goalAway, homeOdds, drawOdds, awayOdds, DateTime.Now);
                    float playerRating = r.Next(30, 80);
                    for (int j = 0; j < 11; j++)
                    {
                        float randomPlusOrMinus = r.Next(-10, 10);
                        Player p = new Player(j, playerRating + randomPlusOrMinus);
                        match.team1.Add(p);
                    }
                    for (int j = 0; j < 11; j++)
                    {
                        float randomPlusOrMinus = r.Next(-10, 10);
                        Player p = new Player(j, playerRating + randomPlusOrMinus);
                        match.team2.Add(p);
                    }
                    if (i < 22000)
                    {
                        idealTestDataMatches.Add(match);
                    }
                    else
                    {
                        idealTestDataMatchesForTest.Add(match);
                    }
                }
                else if (randomNumber == 2)
                {
                    int goalHome = 0;
                    int goalAway = 0;
                    while (goalHome >= goalAway)
                    {
                        goalHome = r.Next(1, 15);
                        goalAway = r.Next(1, 15);
                    }
                    float awayOdds = (float)r.NextDouble() + r.Next(1, 3);
                    float homeOdds = (float)r.NextDouble() + r.Next(3, 10);
                    float drawOdds = (float)r.NextDouble() + r.Next(3, 10);
                    Match match = new Match(i, goalHome, goalAway, homeOdds, drawOdds, awayOdds, DateTime.Now);
                    for (int j = 0; j < 11; j++)
                    {
                        Player p = new Player(j, r.Next(20, 50));
                        match.team1.Add(p);
                    }
                    for (int j = 0; j < 11; j++)
                    {
                        Player p = new Player(j, r.Next(50, 100));
                        match.team2.Add(p);
                    }
                    if (i < 22000)
                    {
                        idealTestDataMatches.Add(match);
                    }
                    else
                    {
                        idealTestDataMatchesForTest.Add(match);
                    }
                }
            }
            Console.ReadKey();
        }
        public void TrainData()
        {
            DeepNetwork network = new DeepNetwork();
            DeepNetwork.matches = idealTestDataMatches;
            network.loadInputAndOutput();
            network.Train();
        }
        public void WriteTestData()
        {
            StreamWriter sw = new StreamWriter("testData.txt");
            foreach (var item in idealTestDataMatches)
            {
                List<float> team1ListRating = new List<float>();
                List<float> team1ListID = new List<float>();
                List<float> team2ListRating = new List<float>();
                List<float> team2ListID = new List<float>();
                foreach (var team1 in item.team1)
                {
                    team1ListID.Add(team1.playerId);
                    team1ListRating.Add(team1.overAllRating);
                }
                sw.Write($"{item.goalHome};{item.goalAway};{item.matchId};{item.betOddsHome};{item.betOddsDraw};{item.betOddsAway};{item.time};");
                for (int i = 0; i < team1ListRating.Count(); i++)
                {
                    sw.Write($"{team1ListID[i]};{team1ListRating[i]};");
                }
                foreach (var team2 in item.team2)
                {
                    team2ListID.Add(team2.playerId);
                    team2ListRating.Add(team2.overAllRating);
                }
                for (int i = 0; i < team2ListRating.Count(); i++)
                {
                    sw.Write($"{team2ListID[i]};{team2ListRating[i]};");
                }
                sw.Write("\n");
            }
            sw.Close();
            sw = new StreamWriter("testDataForTesting.txt");

            foreach (var item in idealTestDataMatchesForTest)
            {
                List<float> team1ListRating = new List<float>();
                List<float> team1ListID = new List<float>();
                List<float> team2ListRating = new List<float>();
                List<float> team2ListID = new List<float>();
                foreach (var team1 in item.team1)
                {
                    team1ListID.Add(team1.playerId);
                    team1ListRating.Add(team1.overAllRating);
                }
                sw.Write($"{item.goalHome};{item.goalAway};{item.matchId};{item.betOddsHome};{item.betOddsDraw};{item.betOddsAway};{item.time};");
                for (int i = 0; i < team1ListRating.Count(); i++)
                {
                    sw.Write($"{team1ListID[i]};{team1ListRating[i]};");
                }
                foreach (var team2 in item.team2)
                {
                    team2ListID.Add(team2.playerId);
                    team2ListRating.Add(team2.overAllRating);
                }
                for (int i = 0; i < team2ListRating.Count(); i++)
                {
                    sw.Write($"{team2ListID[i]};{team2ListRating[i]};");
                }
                sw.Write("\n");
            }
            sw.Close();
        }
    }
}
