﻿using System.Data.SQLite;
using System;
using System.IO;
using System.Collections.Generic;
using CNTK;
using System.Linq;
using System.Diagnostics;

namespace EuropeanSoccerPrediction
{
    class Program
    {
        static List<Match> Matches = new List<Match>();
        static List<Match> MatchesForTesting = new List<Match>();
        static List<Match> LoadFromText(string name)
        {
            int homeWon = 0;
            int draw = 0;
            int awayWon = 0;
            List<Match> matches = new List<Match>();
            StreamReader sr = new StreamReader(name);
            int matchCount = 0;
            while (!sr.EndOfStream)
            {
                bool isNanOrNot = false;
                string[] row = sr.ReadLine().Split(";");
                int goalHome = int.Parse(row[0]);
                int goalAway = int.Parse(row[1]);
                int matchId = int.Parse(row[2]);
                float homeOdds = float.Parse(row[3]);
                float drawOdds = float.Parse(row[4]);
                float awayOdds = float.Parse(row[5]);
                DateTime time = DateTime.Parse(row[6]);
                Match match = new Match(matchId, goalHome, goalAway, homeOdds, drawOdds, awayOdds, time);
                for (int i = 0; i < 22; i += 2)
                {
                    int playerID = int.Parse(row[7 + i]);
                    float overAllRating = float.Parse(row[8 + i]);
                    if (float.IsNaN(overAllRating))
                    {
                        isNanOrNot = true;
                    }
                    Player p = new Player(playerID, overAllRating);
                    match.team1.Add(p);
                }
                for (int i = 0; i < 22; i += 2)
                {
                    int playerId = int.Parse(row[29 + i]);
                    float overAllRating = float.Parse(row[30 + i]);
                    if (float.IsNaN(overAllRating))
                    {
                        isNanOrNot = true;
                    }
                    Player p = new Player(playerId, overAllRating);
                    match.team2.Add(p);
                }
                if (isNanOrNot == false)
                {
                    matches.Add(match);
                    matchCount++;
                }
                if (match.resultHome == 1)
                {
                    homeWon++;
                }
                else if (match.resultDraw == 1)
                {
                    draw++;
                }
                else
                {
                    awayWon++;
                }
            }
            Console.WriteLine(matchCount);
            Console.WriteLine($"Home won: {homeWon}");
            Console.WriteLine($"Draw: {draw}");
            Console.WriteLine($"Away won: {awayWon}");
            sr.Close();
            Console.WriteLine("Press a key to start!");
            Console.ReadKey();
            return matches;
        }
        static List<Match> LoadFromTextWithLimit(string name, int limitCount)
        {
            int homeWon = 0;
            int draw = 0;
            int awayWon = 0;
            List<Match> matches = new List<Match>();
            StreamReader sr = new StreamReader(name);
            int matchCount = 0;
            while (!sr.EndOfStream)
            {
                bool isNanOrNot = false;
                string[] row = sr.ReadLine().Split(";");
                int goalHome = int.Parse(row[0]);
                int goalAway = int.Parse(row[1]);
                int matchId = int.Parse(row[2]);
                float homeOdds = float.Parse(row[3]);
                float drawOdds = float.Parse(row[4]);
                float awayOdds = float.Parse(row[5]);
                DateTime time = DateTime.Parse(row[6]);
                Match match = new Match(matchId, goalHome, goalAway, homeOdds, drawOdds, awayOdds, time);
                for (int i = 0; i < 22; i += 2)
                {
                    int playerID = int.Parse(row[7 + i]);
                    float overAllRating = float.Parse(row[8 + i]);
                    if (float.IsNaN(overAllRating))
                    {
                        isNanOrNot = true;
                    }
                    Player p = new Player(playerID, overAllRating);
                    match.team1.Add(p);
                }
                for (int i = 0; i < 22; i += 2)
                {
                    int playerId = int.Parse(row[29 + i]);
                    float overAllRating = float.Parse(row[30 + i]);
                    if (float.IsNaN(overAllRating))
                    {
                        isNanOrNot = true;
                    }
                    Player p = new Player(playerId, overAllRating);
                    match.team2.Add(p);
                }
                if (isNanOrNot == false)
                {
                    if (match.resultHome == 1)
                    {
                        if (homeWon < limitCount)
                        {
                            homeWon++;
                            matches.Add(match);
                            matchCount++;
                        }
                    }
                    else if (match.resultDraw == 1)
                    {
                        if (draw < limitCount)
                        {
                            draw++;
                            matches.Add(match);
                            matchCount++;
                        }
                    }
                    else
                    {
                        if (awayWon < limitCount)
                        {
                            awayWon++;
                            matches.Add(match);
                            matchCount++;
                        }
                    }
                }
            }
            Console.WriteLine(matchCount);
            Console.WriteLine($"Home won: {homeWon}");
            Console.WriteLine($"Draw: {draw}");
            Console.WriteLine($"Away won: {awayWon}");
            sr.Close();
            Console.ReadKey();
            return matches;
        }
        static void LoadAndTrainRealData(string name)
        {
            Matches = LoadFromText(name+".txt");
            MatchesForTesting = LoadFromText(name + "ForTesting.txt");
            DeepNetwork network = new DeepNetwork();
            DeepNetwork.matches = Matches;
            DeepNetwork.matchesToTest = MatchesForTesting;
            network.loadInputAndOutput();
            network.Train();
            network.loadTestInputAndOutputData();
            network.TestData();
        }
        static void LoadAndTrainRealDataWithLimit(string name, int limit)
        {
            Matches = LoadFromTextWithLimit(name + ".txt", limit);
            MatchesForTesting = LoadFromText(name + "ForTesting.txt");
            DeepNetwork network = new DeepNetwork();
            DeepNetwork.matches = Matches;
            DeepNetwork.matchesToTest = MatchesForTesting;
            network.loadInputAndOutput();
            network.Train();
            network.loadTestInputAndOutputData();
            network.TestData();
        }
        static void CreateTestData()
        {
            Test test = new Test();
            test.CreateTestDatas();
            test.WriteTestData();
        }
        static void LoadAndTrainTestData()
        {
            Matches = LoadFromText("testData.txt");
            MatchesForTesting = LoadFromText("testDataForTesting.txt"); 
            DeepNetwork network = new DeepNetwork();
            DeepNetwork.matches = Matches;
            DeepNetwork.matchesToTest = MatchesForTesting;
            network.loadInputAndOutput();
            network.Train();
            network.loadTestInputAndOutputData();
            network.TestData();
        }
        static void Main(string[] args)
        {
            LoadAndTrainRealData("data");
            //LoadAndTrainRealDataWithLimit("data", 4000);
            //LoadAndTrainTestData();
            Console.ReadKey();
        }

    }
}

