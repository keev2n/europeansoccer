﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EuropeanSoccerPrediction
{
    class DeepNetwork
    {
        public static List<Match> matches { get; set; }
        public static List<Match> matchesToTest { get; set; }

        List<float> inputs = new List<float>();
        List<float> outputs = new List<float>();
        List<float> testInputs = new List<float>();
        List<float> testOutputs = new List<float>();

        static float betOddsHomeMin = -1;
        static float betOddsHomeMax = -1;
        static float betOddsDrawMin = -1;
        static float betOddsDrawMax = -1;
        static float betOddsAwayMin = -1;
        static float betOddsAwayMax = -1;
        static float playerMin = -1;
        static float playerMax = -1;

        readonly int[] layers = new int[] { Config.inputSize, 20, 20, 20, Config.outputSize };
        readonly Variable x;
        readonly Function y;
        public DeepNetwork()
        {
            x = Variable.InputVariable(new int[] { layers[0] }, DataType.Float);

            Function lastLayer = x;
            for (int i = 0; i < layers.Length - 1; i++)
            {
                Parameter weight = new Parameter(new int[] { layers[i + 1], layers[i] }, DataType.Float, CNTKLib.GlorotNormalInitializer());
                Parameter bias = new Parameter(new int[] { layers[i + 1] }, DataType.Float, CNTKLib.GlorotNormalInitializer());

                Function times = CNTKLib.Times(weight, lastLayer);
                Function plus = CNTKLib.Plus(times, bias);

                if (i != layers.Length - 2)
                    lastLayer = CNTKLib.Sigmoid(plus);
                else
                    lastLayer = CNTKLib.Softmax(plus);
            }

            y = lastLayer;
        }
        public static List<float> normalizeData(Match match)
        {
            if (betOddsHomeMax == -1)
            {
                betOddsHomeMin = matches.Min(x => x.betOddsHome);
                betOddsHomeMax = matches.Max(x => x.betOddsHome);
                betOddsDrawMin = matches.Min(x => x.betOddsDraw);
                betOddsDrawMax = matches.Max(x => x.betOddsDraw);
                betOddsAwayMin = matches.Min(x => x.betOddsAway);
                betOddsAwayMax = matches.Max(x => x.betOddsAway);

                float minRatingPlayerTeam1 = matches.Min(x => x.team1.Min(x => x.overAllRating));
                float minRatingPlayerTeam2 = matches.Min(x => x.team2.Min(x => x.overAllRating));

                float maxRatingPlayerTeam1 = matches.Max(x => x.team2.Max(x => x.overAllRating));
                float maxRatingPlayerTeam2 = matches.Max(x => x.team2.Max(x => x.overAllRating));

                if (minRatingPlayerTeam1 < minRatingPlayerTeam2)
                    playerMin = minRatingPlayerTeam1;
                else
                    playerMin = minRatingPlayerTeam2;

                if (maxRatingPlayerTeam1 > maxRatingPlayerTeam2)
                    playerMax = maxRatingPlayerTeam1;
                else
                    playerMax = maxRatingPlayerTeam2;
            }

            List<float> normalizedData = new List<float>();
            normalizedData.Add((match.betOddsHome - betOddsHomeMin) / (betOddsHomeMax - betOddsHomeMin));
            normalizedData.Add((match.betOddsDraw - betOddsDrawMin) / (betOddsDrawMax - betOddsDrawMin));
            normalizedData.Add((match.betOddsAway - betOddsAwayMin) / (betOddsAwayMax - betOddsAwayMin));
            foreach (var item in match.team1)
            {
                normalizedData.Add((item.overAllRating - playerMin) / (playerMax - playerMin));
            }

            foreach (var item in match.team2)
            {
                normalizedData.Add((item.overAllRating - playerMin) / (playerMax - playerMin));
            }

            return normalizedData;
        }
        public void loadTestInputAndOutputData()
        {
            foreach (var item in matchesToTest)
            {
                testInputs.Add(item.betOddsHome);
                testInputs.Add(item.betOddsDraw);
                testInputs.Add(item.betOddsAway);
                foreach (var team1Player in item.team1)
                {
                    testInputs.Add(team1Player.overAllRating);
                }
                foreach (var team2Player in item.team2)
                {
                    testInputs.Add(team2Player.overAllRating);
                }
                testOutputs.Add(item.resultHome);
                testOutputs.Add(item.resultDraw);
                testOutputs.Add(item.resultAway);
            }
        }
        public void loadInputAndOutput()
        {
            foreach (var item in matches)
            {
                List<float> normalizedData = normalizeData(item);
                for (int i = 0; i < normalizedData.Count(); i++)
                {
                    inputs.Add(normalizedData[i]);
                }
                outputs.Add(item.resultHome);
                outputs.Add(item.resultDraw);
                outputs.Add(item.resultAway);
            }
        }
        public void TestData()
        {
            double calculated = 0;
            double calculatedHome = 0;
            double notCalculatedHome = 0;
            double calculatedDraw = 0;
            double notCalculatedDraw = 0;
            double calculatedAway = 0;
            double notCalculatedAway = 0;

            int iThinkHomeWin = 0;
            int iThinkDraw = 0;
            int iThinkAwayWin = 0;
            foreach (var item in matchesToTest)
            {
                float[] predictionResult = Prediction(item);
                float predictionHome = predictionResult[0];
                float predictionDraw = predictionResult[1];
                float predictionAway = predictionResult[2];
                if (predictionHome > predictionDraw && predictionHome > predictionAway)
                {
                    iThinkHomeWin++;
                }
                else if (predictionDraw > predictionHome && predictionDraw > predictionAway)
                {
                    iThinkDraw++;
                }
                else
                {
                    iThinkAwayWin++;
                }

                if (item.resultHome == 1)
                {
                    if (predictionHome > predictionDraw && predictionHome > predictionAway)
                    {
                        calculated++;
                        calculatedHome++;
                    }
                    else
                    {
                        notCalculatedHome++;
                    }
                }
                else if (item.resultDraw == 1)
                {
                    if (predictionDraw > predictionHome && predictionDraw > predictionAway)
                    {
                        calculated++;
                        calculatedDraw++;
                    }
                    else
                    {
                        notCalculatedDraw++;
                    }
                }
                else
                {
                    if (predictionAway > predictionHome && predictionAway > predictionDraw)
                    {
                        calculated++;
                        calculatedAway++;
                    }
                    else
                    {
                        notCalculatedAway++;
                    }
                }
            }
            double accuracy = Convert.ToDouble(calculated / matchesToTest.Count());
            double sensitivityHome;
            double sensitivityDraw;
            double sensitivityAway;
            if (calculatedHome != 0 && notCalculatedHome != 0)
            {
                sensitivityHome = calculatedHome / (calculatedHome + notCalculatedHome);
                Console.WriteLine($"Sensitivity home: {sensitivityHome}");
            }
            else
            {
                if (calculatedHome == 0)
                {
                    Console.WriteLine("No calculated Home");
                }
                else
                {
                    Console.WriteLine("Every home calculated");
                }
            }

            if (calculatedDraw != 0 && notCalculatedDraw != 0)
            {
                sensitivityDraw = calculatedDraw / (calculatedDraw + notCalculatedDraw);
                Console.WriteLine($"Sensitivity draw: {sensitivityDraw}");
            }
            else
            {
                if (calculatedDraw == 0)
                {
                    Console.WriteLine("No calculated Draw");
                }
                else
                {
                    Console.WriteLine("Every Draw calculated");
                }
            }

            if (calculatedAway != 0 && notCalculatedAway != 0)
            {
                sensitivityAway = calculatedAway / (calculatedAway + notCalculatedAway);
                Console.WriteLine($"Sensitivity Away: {sensitivityAway}");
            }
            else
            {
                if (calculatedAway == 0)
                {
                    Console.WriteLine("No calculated Away");
                }
                else
                {
                    Console.WriteLine("Every Away calculated");
                }
            }
            Console.WriteLine($"Accuracy: {accuracy}");
            Console.WriteLine($"I think home win {iThinkHomeWin}");
            Console.WriteLine($"I think it's draw {iThinkDraw}");
            Console.WriteLine($"I think it's away win {iThinkAwayWin}");
        }
        public float[] Prediction(Match m)
        {
            List<float> normalizedData = normalizeData(m);
            float[] results = new float[3];
            Value x_value = Value.CreateBatch(x.Shape, new float[] { normalizedData[0], normalizedData[1], normalizedData[2], normalizedData[3], normalizedData[4],
                normalizedData[5], normalizedData[6], normalizedData[7], normalizedData[8], normalizedData[9], normalizedData[10],
                normalizedData[11], normalizedData[12], normalizedData[13], normalizedData[14],  normalizedData[15],
                normalizedData[16], normalizedData[17], normalizedData[18], normalizedData[19], normalizedData[20], normalizedData[21],
                normalizedData[22], normalizedData[23], normalizedData[24]}, DeviceDescriptor.CPUDevice);
            var inputDataMap2 = new Dictionary<Variable, Value>()
                {
                        { x, x_value },
                };
            var outputDataMap = new Dictionary<Variable, Value>() { { y, null } };
            y.Evaluate(inputDataMap2, outputDataMap, DeviceDescriptor.CPUDevice);
            float resultHome = outputDataMap[y].GetDenseData<float>(y)[0][0];
            float resultDraw = outputDataMap[y].GetDenseData<float>(y)[0][1];
            float resultAway = outputDataMap[y].GetDenseData<float>(y)[0][2];
            results[0] = resultHome;
            results[1] = resultDraw;
            results[2] = resultAway;
            return results;
        }
        public void Train()
        {
            int dataSetCount = matches.Count();
            Variable yt = Variable.InputVariable(new int[] { Config.outputSize }, DataType.Float);
            Function loss = CNTKLib.CrossEntropyWithSoftmax(y, yt);
            Function error = CNTKLib.ClassificationError(y, yt);

            Learner learner = CNTKLib.SGDLearner(new ParameterVector(y.Parameters().ToArray()), new TrainingParameterScheduleDouble(Config.LearningRate, Config.batchSize));
            Trainer trainer = Trainer.CreateTrainer(y, loss, error, new List<Learner>() { learner });

            for (int epochI = 0; epochI <= Config.epochCount; epochI++)
            {
                double sumLoss = 0;
                double sumError = 0;

                for (int batchI = 0; batchI < dataSetCount / Config.batchSize; batchI++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, inputs.GetRange(batchI * Config.batchSize * Config.inputSize, Config.batchSize * Config.inputSize), DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, outputs.GetRange(batchI * Config.batchSize * Config.outputSize, Config.batchSize * Config.outputSize), DeviceDescriptor.CPUDevice);
                    var inputDataMap = new Dictionary<Variable, Value>()
                    {
                        { x, x_value },
                        { yt, yt_value }
                    };

                    trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
                    sumLoss += trainer.PreviousMinibatchLossAverage() * trainer.PreviousMinibatchSampleCount();
                    sumError += trainer.PreviousMinibatchEvaluationAverage() * trainer.PreviousMinibatchSampleCount();
                }
                Console.WriteLine(String.Format("{0}\t{1}\t{2}", epochI, sumLoss / dataSetCount, 1.0 - sumError / dataSetCount));
            }
        }
    }
}
