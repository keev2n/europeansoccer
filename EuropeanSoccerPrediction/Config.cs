﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EuropeanSoccerPrediction
{
    static class Config
    {
        public const int batchSize = 50;
        public const int inputSize = 25;
        public const int outputSize = 3;
        public const int epochCount = 300;
        public const double LearningRate = 0.1;
    }
}
