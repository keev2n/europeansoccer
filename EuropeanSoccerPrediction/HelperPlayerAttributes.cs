﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EuropeanSoccerPrediction
{
    class HelperPlayerAttributes
    {
        static public List<HelperPlayerAttributes> helperPlayerAttributes = new List<HelperPlayerAttributes>();
        public int PlayerApiID { get; set; }
        public float OverallRating { get; set; }
        public DateTime Time { get; set; }
        public HelperPlayerAttributes(int PlayerApiID, float OverallRating, DateTime Time)
        {
            this.PlayerApiID = PlayerApiID;
            this.OverallRating = OverallRating;
            this.Time = Time;
        }
    }
}
